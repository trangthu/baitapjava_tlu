package Tuan1;

import java.util.Scanner;

public class BaiTap {
    //Bai1
    public static class ThongTinCaNhan {
        public static void main(String[] args) {
            System.out.println("Nguyen Thi Trang Thu\tA25763\tTI27\tthuntt2902@gmail.com");
            for (int i = 9; i >= 1; i--) {
                System.out.println(i + " bottles of beer on the wall, " + i + "bottles of beer.");
                System.out.println("Take one down, pass it around,");
            }
        }
    }

    //Bai2
    //2.1. Tim UCLN cua 2 so nguyen a,b
    public static int nhap() {
        Scanner input = new Scanner(System.in);
        boolean check = false;
        int n = 0;
        while (!check) {
            System.out.print(" ");
            try {
                n = input.nextInt();
                check = true;
            } catch (Exception e) {
                System.out.println("Ban hay nhap lai du lieu kieu so");
                input.nextLine();
            }
        }
        return (n);
    }

    public static void UCLN(int a, int b) {
        if (a < 0)
            a = -a;
        if (b < 0)
            b = -b;
        if (a == 0 && b == 0)
            System.out.println("Khong co uoc so chung lon nhat");
        else {
            while (a != 0 && b != 0) {
                if (a > b)
                    a = a % b;
                else
                    b = b % a;
            }
            if (a == 0)
                System.out.println(b);//b la USCLN.
            else
                System.out.println(a);//a la USCLN.
        }
    }

    //2.2. Tim so fibonaci thu n
    public static int fibonaci(int n) {
        if ((n == 1) || (n == 2)) {
            return 1;
        } else {
            int arr[] = new int[n];
            arr[0] = 1;
            arr[1] = 1;
            for (int i = 2; i < arr.length; i++) {
                arr[i] = arr[i - 1] + arr[i - 2];
            }
            return arr[n - 1];
        }

    }

    public static void main(String[] args) {
        //2.1
        System.out.println("Nhap a");
        int a = nhap();
        System.out.println("Nhap b");
        int b = nhap();
        System.out.println("Uoc chung lon nhat cua " + a + " va " + b + " la:");
        UCLN(a, b);
        //2.2
        System.out.println("So Fibonaci: " + fibonaci(3));
    }
}
