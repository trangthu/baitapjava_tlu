package Tuan4;

public class AnimalTest {
    public static void main (String[]args){
        Animal a = new Animal("Cho");
        System.out.println(a.getName());
        a.sayHello();
        Cat c =new Cat("susi");
        c.selfIntroduce();
    }
}
class Animal{
    String name;

    public Animal(){
        name="no name";
    }
    public Animal(String a){
        name=a;
    }
    public String getName(){
        return name;
    }
    public void sayHello(){
        System.out.println("Well… I don’t know what to say.");
    }
    public void selfIntroduce(){
        System.out.println("My name is "+getName()+". I am a "+this.getClass().getSimpleName());
    }
}

class Cat extends Animal{
    public Cat(){
        super();
    }

    public Cat(String a) {
        super(a);
    }

    public void sayHello(){
        System.out.println("Meow…");
    }

}

class Cow extends Animal{
    public Cow(){
        super();
    }
    public Cow(String a) {
        super(a);
    }

    public void sayHello(){
        System.out.println("Mooo…");
    }
}