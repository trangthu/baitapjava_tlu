package Tuan4;
import Tuan2.MyDate;

public class PeopleTest {
    public static void main (String[]args){
        // 1a
        Employee newbie = new Employee();
        newbie.name = "Newbie";
        newbie.birthday = new MyDate(10,2,1989);
        newbie.salary = 1000000;
        System.out.println(newbie.toString());

        Manager boss = new Manager();
        boss.name="Boss";
        boss.birthday=new MyDate(23,2,1979);
        boss.salary=4000000;
        System.out.println(boss.toString());

        boss.setAssistant(newbie);

        Manager bigBoss = new Manager();
        bigBoss.name="Big Boss";
        bigBoss.birthday=new MyDate(3,12,1969);
        bigBoss.salary=10000000;
        System.out.println(bigBoss.toString());

        bigBoss.setAssistant(boss);

        //1b
        Person [] p ={newbie,boss,bigBoss};
        for(int i=0;i<3;i++){
            System.out.println(p[i].toString());
        }

        //1c

    }
}
class Person extends MyDate{
    String name;
    MyDate birthday;

    public String getname(){
        return name;
    }
    public String toString(){
        return name + "" + birthday.in();
    }

}

class Employee extends Person{
    double salary;

    public double getSalary(){
        return salary;
    }
    public String toString(){
        return "'"+getname() +"'"+ "," + "("+birthday.in()+")"+","+salary;
    }
}

class Manager extends Employee{
    Employee assistant;

    public void setAssistant(Employee e){
        assistant = e;
    }
    public String toString(){
        return "'"+getname()+"'"+ "," + "("+birthday.in()+")"+","+salary;
    }
}