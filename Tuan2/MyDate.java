package Tuan2;

import java.util.Scanner;

public class MyDate {
    private int date;
    private int month;
    private int year;
    private Scanner input;

    public MyDate() {
    }

    public MyDate(int d,int m,int y) {
        date=d;
        month=m;
        year=y;
    }
    public String in(){
       return date+","+month+","+year;
    }
    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void nhap(int d, int m, int y) {
        int date_max = 0;
        input = new Scanner(System.in);
        System.out.println("Nhap ngay:");
        d = input.nextInt();
        System.out.println("Nhap thang:");
        m = input.nextInt();
        System.out.println("Nhap nam:");
        y = input.nextInt();
        if (y < 0 || m < 0 || m > 12 || d < 0 || d > 31) {
            System.out.println("Ngay/Thang/Nam khong hop le");
        } else {
            switch (m) {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    date_max = 31;
                    break;
                case 2:
                    if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0))
                        date_max = 29;
                    else
                        date_max = 28;
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    date_max = 30;
                    break;
            }
            if (d <= date_max) {
                System.out.println(d + "/" + m + "/" + y);
            } else {
                System.out.println("Ngay/Thang/Nam khong hop le");
            }
        }

    }
    public static void main(String[] args) {
        int d = 0, m = 0, y = 0;
        MyDate demo = new MyDate();
        demo.nhap(d, m, y);
    }

}
