package Tuan2;
import java.util.Scanner;

public class PhanSo {
	private int TS;
	private int MS;
	private Scanner input;
	
	public PhanSo() {
		this.TS=0;
		this.MS=1;
	}
	public PhanSo(int ts) {
		this.TS=ts;
		this.MS=1;
	}
	public PhanSo(int ts, int ms) {
		this.TS=ts;
		this.MS=ms;
	}
	
	public int getMS() {
		return MS;
	}
	public void setMS(int mS) {
		MS = mS;
	}
	public int getTS() {
		return TS;
	}
	public void setTS(int tS) {
		TS = tS;
	}
	
	public void RutGonPS() {
		int max,min,ucln;
		if (TS > 0)
		{
			max = TS > MS ? TS : MS;
			min = TS < MS ? TS : MS;
			ucln = max % min;
			while (ucln > 0)
			{
				max = min;
				min = ucln;
				ucln = max % min;
			}
			ucln = min;
			TS = TS / ucln;
			MS = MS / ucln;
		}
		else
			if (TS == 0)
				return ;
			else
			{
				TS = TS*(-1);
				max = TS > MS ? TS : MS;
				min = TS < MS ? TS : MS;
				ucln = max % min;
				while (ucln > 0)
				{
					max = min;
					min = ucln;
					ucln = max % min;
				}
				ucln = min;
				TS = TS / ucln;
				MS = MS / ucln;
				TS = TS*(-1);
			}
		
	}
	
	public void nhap() {
		input = new Scanner(System.in);
		System.out.println("Nhap tu so ");
		TS=input.nextInt();
		System.out.println("Nhap mau so ");
		MS=input.nextInt();
	}
	
	public void in() {
		if(TS == 0)
			System.out.println(0);
		else {
			if(TS == 1 && MS ==1)
				System.out.println(1);
			else
				System.out.println(TS+"/"+MS);
		}
	}
	
	public PhanSo Cong (PhanSo ps) {
		PhanSo x = new PhanSo();
		x.TS= (TS*ps.getMS() + ps.getTS()*MS);
		x.MS= MS*ps.getMS();
		x.RutGonPS();
		return x;
	}
	
	public PhanSo Tru(PhanSo ps) {
		PhanSo x = new PhanSo();
		x.TS= (TS*ps.getMS() - ps.getTS()*MS);
		x.MS= MS*ps.getMS();
		x.RutGonPS();
		return x;
	}
	
	public PhanSo Nhan(PhanSo ps) {
		PhanSo x = new PhanSo();
		x.TS= TS*ps.getTS();
		x.MS= MS*ps.getMS();
		x.RutGonPS();
		return x;
	}
	
	public PhanSo Chia(PhanSo ps) {
		PhanSo x = new PhanSo();
		x.TS= TS*ps.getMS();
		x.MS= MS*ps.getTS();
		x.RutGonPS();
		return x;
	}
	
	public static void main(String [] args) {
		PhanSo x = new PhanSo();
		PhanSo y = new PhanSo();
		System.out.println("Nhap phan so thu 1");
		x.nhap();
		System.out.println("Nhap phan so thu 2");
		y.nhap();
		//Cong
		PhanSo z= x.Cong(y);
		z.RutGonPS();
		System.out.println("Tong 2 phan so:");
		z.in();
		//Tru
		PhanSo z1= x.Tru(y);
		z1.RutGonPS();
		System.out.println("Hieu 2 phan so:");
		z1.in();
		//Nhan
		PhanSo z2= x.Nhan(y);
		z2.RutGonPS();
		System.out.println("Tich 2 phan so:");
		z2.in();
		//Chia
		PhanSo z3= x.Chia(y);
		z3.RutGonPS();
		System.out.println("Thuong 2 phan so:");
		z3.in();
	}
}
